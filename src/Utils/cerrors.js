export default (e, next) => {
    function ClientException(message, code = 1, errs = null) {
        this.message = message;
        /* 1 - syntax, 2 - validation/form */
        this.code = code;
        this.errList = errs
    }
    try {
        if (typeof e === "object") {
            if (e.response && e.response.data) {
                let reason = e.response.data

                if (typeof reason === "object"){
                    if (reason.validations) {
                        let errors = {}
                        for (let key in reason.validations) {
                            errors[key] = reason.validations[key].join(', ')
                        }
                        throw new ClientException(("Por favor, corriga os campos com erro" || reason.prettyErr), 2, errors)
                    } else if (reason.message) {
                        throw new ClientException(reason.prettyErr || reason.message, 2)
                    }
                } else {
                    throw new ClientException(reason.toString(), 2)
                }
            } else if (e.message && e.stack) {
                throw new ClientException(e.message)
            }
        }
        else {
            throw new ClientException(e.toString(), 2)
        }
    } catch (ex) {
        if (ex.code == 1) console.log({ except: ex })
        next((ex.code == 2 ? ex.message : "erro desconhecido"), ex.errList)
    }
}