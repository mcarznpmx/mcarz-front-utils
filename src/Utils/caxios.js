import axios from 'axios'

export default (baseURL, customParams) => {
    
    let instance = axios.create({ baseURL })

    // axios.interceptors.response.use(res => res.data, error => Promise.Reject(error))

    instance.interceptors.request.use(config => {
        if (!config.params) {
            config.params = {}
        }
        
        if (customParams){
            for (i in customParams) {
                config.params[i] = customParams[i]
            }
        }

        return config;
    }, error => Promise.Reject(error))

    return instance
}